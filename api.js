const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const userRoutes = require("./routes/userRoutes");
const authRoutes = require("./routes/authRoutes");

const app = express();

// Allow the backend to read req.body
app.use(bodyParser.json());

// Connect to MongoDB
mongoose.connect("mongodb://localhost:27017/authapidb", {useNewUrlParser: true, useCreateIndex: true})
const db = mongoose.connection;
// Handle DB errors
db.on("error", (error)=>{
    console.log("MongoDB connection error: " + error);
});

// Called once we are connected to MongoDB
db.once("open", ()=>{
    console.log("Connected to mongoDB: mongodb://localhost:27017/authapidb");
})

// Routes
app.use("/user", userRoutes);
app.use("/auth", authRoutes);

// Allow access from anywhere
app.use(cors());

// Error handler for to handle 404 errors
app.use((req, res, next)=>{
    // Create the error object
    let error = new Error("API endpoint not found");
    error.state = 404;
    next(error);
});
// Global Error Handler
app.use((error, req, res, next)=>{

    res.status(error.status || 500);
    // Send the JSON error back to the client
    res.json(
    {
       error: {message: error.message}
    });
});

// Listen on TCP/3000
app.listen(3000, ()=>{
    console.log("Authentication API listening on TCP/3000");
})