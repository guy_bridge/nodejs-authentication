const jwt = require("jsonwebtoken");
const SECRET_KEY = "p@$$w0rd2020";
const User = require("../model/User").User;

// Middleware to verify a user's supplied JsonWebToken
checkToken = (req, res, next) => 
{
    if(req.headers.token)
    {
        jwt.verify(req.headers.token, SECRET_KEY, (error, decoded)=>{
            if(error)
            {
                let err = new Error("Token invalid or expired");
                err.status = 401;
                return next(err);
            }
            else
            {
                res.token = decoded;
                return next();
            }

        });
    }
}

const getToken = (id)=>
{
    const token = jwt.sign(
        {id: id},
        SECRET_KEY,
        {expiresIn: 86400});

        return token;
}

module.exports = {checkToken, getToken};