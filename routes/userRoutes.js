const User = require("../model/User").User;
const express = require("express");
const router = express.Router();
const authHelper = require("../helper/authHelper");

// Sign up route
router.post("/signup", (req, res, next)=>{
    
    
    if(req.body.email && req.body.password)
    {
        let userJson = {
            email: req.body.email
        };

        // Generate a password hash
        User.hashPassword(req.body.password)
            .then(hash => {

                userJson.password = hash;

                User.create(userJson, (error, user)=>{
                    if(user) return res.json(
                        {result: "Sign up success",
                        token: authHelper.getToken(user._id)});
                    return next(error);
                });
            })
            .catch(error => {
                return next(error);
            })

     
    }
    else
    {
        return next(new Error("Missing email or password in sign up body"));
    }

});

router.get("/profile", authHelper.checkToken, (req, res, next)=>
{
    User.findOne({_id: res.token.id}).exec()
        .then(user => {
            res.json(user);
        })  
        .catch(error => {
            return next(error);
        })
})

module.exports = router;