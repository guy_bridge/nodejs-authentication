// User model object to store a user in the database

const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstName: String,
    lastName: String,
    email:{type: String, required: true, unique: true, trim: true},
    password: {type: String, required: true},
    createdAt: {type: Date, default: Date.now}
});

// Static schema method to authenicate a user against the database
userSchema.statics.authenticate = (email, password)=>
{
    return new Promise((resolve, reject)=>{
        User.findOne({email: email}).exec()
            .then(user => {
                
                if(!user)
                {
                    let err = new Error("User not found");
                    err.status = 401;
                    return reject(err);
                }

                bcrypt.compare(password, user.password, (error, result)=>{
                    if(result)
                    {
                        resolve(user);
                    }
                    else{
                        let err = new Error("Password incorrect");
                        err.status = 401;
                        reject(err);
                    }
                })

            })
            .catch(error => {
                reject(error);
            }) 
    });
}

// Method to hash a password
// This should be called before saving the password to the database
userSchema.statics.hashPassword = (password) =>
{
    return new Promise((resolve, reject)=>{
        bcrypt.hash(password, 10, (err, hash)=>{
            if(!err) return resolve(hash);
            return reject(err);
        });
    });
}


const User = mongoose.model("User", userSchema);
module.exports.User = User;