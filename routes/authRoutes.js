const User = require("../model/User").User;

const express = require("express");
const router = express.Router();
const authHelper = require("../helper/authHelper");

// Auth users
router.post("/login", (req, res, next)=>{
    
    User.authenticate(req.body.email, req.body.password)
        .then(user =>{
            
            const token = authHelper.getToken(user._id);
            res.json({result: "Authentication success", token: token});

        })
        .catch(error =>{
            next(error);
        })

});

module.exports = router;